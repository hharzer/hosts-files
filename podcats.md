# Liste für Podcasts

## Spaß/Comedy/Advice

* [I I Were You](https://pca.st/ifiwereyou)
* [My Brother, my Bother and Me](https://pca.st/mybrother)
* [Frienshipping!](https://pca.st/5rXO)
* [Do By Friday](https://pca.st/dobyfriday)

## Gesellschaft/Poduktivität

* [Back to Work](https://pca.st/Wn7NRK)
* [Disruption](https://pca.st/disruption)
* [Hello Internet](https://pca.st/hellointernet)
* [Trends Like These](https://pca.st/trends)

## Spiele

* [DLC](https://pca.st/Cuhg)
* [Remaster](https://pca.st/remaster)
* [What's Good Games](https://pca.st/whatsgoodgames)

## DnD

* [Critical Role](https://pca.st/AO0c)
* [Quest Quest](https://pca.st/3z0X)
* [The Adventure Zone](https://pca.st/adventurezone)