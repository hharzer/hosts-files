# Hosts file cleaned 

A cleaned version of [StevenBlack's](https://github.com/StevenBlack/hosts) host file _Unified hosts + fakenews + gambling + porn_

All empty lines and comments were cleaned up so that Spotify (main reason, there may be other apps that benefit from removing them) is usable again. 
Previously, only search and playlists would work because the connection was not allowed somehow. Seems to be a problem on Spotify's side